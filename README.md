# Simple OAuth Refresh Token Buffer

## Contents of this file

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Maintainers](#maintainers)

## Introduction

This module extends the `simple_oauth` module by allowing a per-client
configurable grace period for the refresh token rotation.

By allowing the previous refresh token to still be used within a small time
frame (the grace period), the user experience is massively improved.

**Real world example:**

A frontend application has both an access token and a refresh token.
The application is configured in a way to automatically refresh both tokens,
when the access token is expired.

Due to the nature of web applications, when the user comes back to the
application after some time, where the access token has expired, but the refresh
token is still valid, multiple requests may be triggered that all try to refresh
the tokens.
This leads to the first request being successful, but the other requests failing
due to all requests using the exact same refresh token.

With this module enabled and configured on the client, the subsequent requests
would still be successful, as the refresh token would return the same token pair
issued in the first request.

> :warning: **WARNING**: This does impose a minor security risk, where an
> attacker could potentially get a new token pair within the grace period
> without the system noticing.
>
> However, the attack vector is limited to the grace period, which is
> configurable per-client. Ultimately, it is up to the developer to find the
> right balance between security and user experience.

**Confidence in using this functionality:**

Despite the minor security implications, even big auth providers like **Auth0**
and **Okta** provide this functionality.

See here:
  - [Auth0 Refresh Token Rotation Overlap Period](https://auth0.com/docs/secure/tokens/refresh-tokens/configure-refresh-token-rotation#configure-in-the-dashboard)
  - [Okta Refresh Token Rotation Grace Period](https://developer.okta.com/docs/guides/refresh-tokens/main/#grace-period-for-token-rotation)

> **Note:** The default grace period of 30 seconds was taken from Okta.

### Usage

To use this module, simply enable the **Refresh Token** grant type in your
OAuth2 Consumer and enable the **Refresh Token Buffer** functionality in the
**Additional Settings** tab.

The grace period must be between 1 and 60 seconds.

> **Note:** Setting the grace period to 0 will disable the functionality.

## Requirements

The core module requires Drupal 10.3 or 11 and the following contrib modules:

- [Simple OAuth](https://www.drupal.org/project/simple_oauth)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Maintainers

Current maintainers:

- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))
