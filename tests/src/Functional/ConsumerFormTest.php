<?php

declare(strict_types=1);

namespace Drupal\Tests\simple_oauth_refresh_token_buffer\Functional;

use Drupal\consumers\Entity\Consumer;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the consumer form validation.
 *
 * @group simple_oauth_refresh_token_buffer
 */
class ConsumerFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'consumers',
    'file',
    'image',
    'options',
    'serialization',
    'system',
    'simple_oauth',
    'simple_oauth_test',
    'user',
    'simple_oauth_refresh_token_buffer',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The test consumer.
   */
  protected Consumer $consumer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create admin user.
    $admin = $this->drupalCreateUser(['administer consumer entities']);
    $this->drupalLogin($admin);

    // Create a test consumer.
    $this->consumer = Consumer::create([
      'label' => 'Test',
      'client_id' => 'test',
      'secret' => 'test',
      'grant_types' => [
        'refresh_token',
      ],
      'refresh_token_buffer_enabled' => TRUE,
      'refresh_token_buffer_grace_period' => 30,
    ]);
    $this->consumer->save();
  }

  /**
   * Tests the grace period validation in the consumer form.
   */
  public function testGracePeriodValidation(): void {
    // Test with invalid values.
    $this->drupalGet("/admin/config/services/consumer/{$this->consumer->id()}/edit");
    $this->submitForm([
      'refresh_token_buffer_enabled[value]' => '1',
      'refresh_token_buffer_grace_period[0][value]' => '-1',
    ], 'Save');
    $this->assertSession()->pageTextContains('The grace period must be between 0 and 60 minutes.');

    $this->drupalGet("/admin/config/services/consumer/{$this->consumer->id()}/edit");
    $this->submitForm([
      'refresh_token_buffer_enabled[value]' => '1',
      'refresh_token_buffer_grace_period[0][value]' => '61',
    ], 'Save');
    $this->assertSession()->pageTextContains('The grace period must be between 0 and 60 minutes.');

    // Test with valid values.
    $this->drupalGet("/admin/config/services/consumer/{$this->consumer->id()}/edit");
    $this->submitForm([
      'refresh_token_buffer_enabled[value]' => '1',
      'refresh_token_buffer_grace_period[0][value]' => '0',
    ], 'Save');
    $this->assertSession()->pageTextContains('Saved the Test consumer.');

    $this->drupalGet("/admin/config/services/consumer/{$this->consumer->id()}/edit");
    $this->submitForm([
      'refresh_token_buffer_enabled[value]' => '1',
      'refresh_token_buffer_grace_period[0][value]' => '30',
    ], 'Save');
    $this->assertSession()->pageTextContains('Saved the Test consumer.');

    $this->drupalGet("/admin/config/services/consumer/{$this->consumer->id()}/edit");
    $this->submitForm([
      'refresh_token_buffer_enabled[value]' => '1',
      'refresh_token_buffer_grace_period[0][value]' => '60',
    ], 'Save');
    $this->assertSession()->pageTextContains('Saved the Test consumer.');
  }

}
