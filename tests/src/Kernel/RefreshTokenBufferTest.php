<?php

declare(strict_types=1);

namespace Drupal\Tests\simple_oauth_refresh_token_bufer\Kernel;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\simple_oauth_refresh_token_buffer\TempStore\RefreshTokenBufferTempStoreFactory;
use Drupal\Tests\simple_oauth\Kernel\AuthorizedRequestBase;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;
use GuzzleHttp\Psr7\Query;
use Symfony\Component\HttpFoundation\Request;

/**
 * Test the refresh token buffer functionality.
 */
class RefreshTokenBufferTest extends AuthorizedRequestBase {

  /**
   * The refresh token.
   */
  protected string $refreshToken;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'consumers',
    'file',
    'image',
    'options',
    'serialization',
    'system',
    'simple_oauth',
    'simple_oauth_test',
    'user',
    'simple_oauth_refresh_token_buffer',
  ];

  /**
   * Last request id.
   */
  protected string $requestId;

  /**
   * The tempstore.
   */
  protected RefreshTokenBufferTempStoreFactory $tempStoreFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->grantPermissions(Role::load(RoleInterface::AUTHENTICATED_ID), [
      'grant simple_oauth codes',
    ]);

    $this->client
      ->set('automatic_authorization', TRUE)
      ->set('refresh_token_buffer_enabled', TRUE)
      ->set('refresh_token_buffer_grace_period', 30)
      ->save();

    $current_user = $this->container->get('current_user');
    $current_user->setAccount($this->user);

    $authorize_url = Url::fromRoute('oauth2_token.authorize')->toString();

    $parameters = [
      'response_type' => 'code',
      'client_id' => $this->client->getClientId(),
      'client_secret' => $this->clientSecret,
      'scope' => $this->scope,
      'redirect_uri' => $this->redirectUri,
    ];
    $request = Request::create($authorize_url, 'GET', $parameters);
    $response = $this->httpKernel->handle($request);
    $parsed_url = parse_url($response->headers->get('location'));
    $parsed_query = Query::parse($parsed_url['query']);
    $code = $parsed_query['code'];
    $parameters = [
      'grant_type' => 'authorization_code',
      'client_id' => $this->client->getClientId(),
      'client_secret' => $this->clientSecret,
      'code' => $code,
      'scope' => $this->scope,
      'redirect_uri' => $this->redirectUri,
    ];
    $request = Request::create($this->url->toString(), 'POST', $parameters);
    $response = $this->httpKernel->handle($request);
    $parsed_response = Json::decode((string) $response->getContent());
    $this->refreshToken = $parsed_response['refresh_token'];

    $this->tempStoreFactory = $this->container->get(RefreshTokenBufferTempStoreFactory::class);
  }

  /**
   * Test functionality with buffer disabled.
   */
  public function testWithoutBufferEnabled() {
    $this->client->set('refresh_token_buffer_enabled', FALSE)->save();

    // First refresh is successful.
    $response = $this->doRefresh();
    $this->assertEquals(200, $response->getStatusCode());

    // Second refresh fails.
    $response = $this->doRefresh();
    $this->assertEquals(400, $response->getStatusCode());
    $this->assertStringContainsString('refresh token is invalid', (string) $response->getContent());

    // Third refresh fails.
    $response = $this->doRefresh();
    $this->assertEquals(400, $response->getStatusCode());
    $this->assertStringContainsString('refresh token is invalid', (string) $response->getContent());
  }

  /**
   * Test functionality with buffer enabled.
   */
  public function testWithBufferEnabled() {
    // First refresh is successful.
    $response = $this->doRefresh();
    $this->assertEquals(200, $response->getStatusCode());
    $this->assertFalse($response->headers->has('X-Buffered'));

    // Second is buffered.
    $response = $this->doRefresh();
    $this->assertEquals(200, $response->getStatusCode());
    $this->assertTrue($response->headers->has('X-Buffered'));

    // Third is buffered.
    $response = $this->doRefresh();
    $this->assertEquals(200, $response->getStatusCode());
    $this->assertTrue($response->headers->has('X-Buffered'));
  }

  /**
   * Test with buffer grace period expired.
   */
  public function testWithBufferGracePeriodExpired() {
    // Set grace period to 1 second.
    $this->client->set('refresh_token_buffer_grace_period', 2)->save();

    // First refresh is successful.
    $response = $this->doRefresh(TRUE);
    $this->assertEquals(200, $response->getStatusCode());

    // Second refresh is within grace period.
    $response = $this->doRefresh(TRUE);
    $this->assertEquals(200, $response->getStatusCode());

    // Wait for 3 seconds.
    sleep(3);

    // Third refresh is outside of grace period.
    $response = $this->doRefresh();
    $this->assertEquals(400, $response->getStatusCode());
  }

  /**
   * Test with expired buffer.
   *
   * This test case makes a legitimate refresh.
   * Then tries to do the same refresh again,
   * but buffered response already expired.
   */
  public function testWithBufferExpired() {
    $tempStore = $this->tempStoreFactory->get($this->client->getClientId(), 30);

    // First refresh is successful.
    $response = $this->doRefresh(TRUE);
    $this->assertEquals(200, $response->getStatusCode());

    // Simulate buffered request being expired.
    $tempStore->delete($this->requestId);

    // Second refresh fails.
    $response = $this->doRefresh();
    $this->assertEquals(400, $response->getStatusCode());
    $this->assertStringContainsString('refresh token is invalid', (string) $response->getContent());
  }

  /**
   * Execute token refresh request and return response.
   */
  protected function doRefresh($setId = FALSE) {
    $parameters = [
      'grant_type' => 'refresh_token',
      'client_id' => $this->client->getClientId(),
      'client_secret' => $this->clientSecret,
      'refresh_token' => $this->refreshToken,
      'scope' => $this->scope,
    ];
    $request = Request::create($this->url->toString(), 'POST', $parameters);
    $request->headers->set('X-Consumer-ID', $this->client->getClientId());

    if ($setId) {
      $this->requestId = hash('sha256', json_encode($request->request->all()));
    }

    // Manually set request time.
    // This is used by the SharedTempStore to determine if
    // the buffer is expired.
    $_SERVER['REQUEST_TIME'] = time();

    $response = $this->httpKernel->handle($request);

    return $response;
  }

}
