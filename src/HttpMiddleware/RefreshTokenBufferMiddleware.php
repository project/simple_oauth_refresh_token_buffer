<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_refresh_token_buffer\HttpMiddleware;

use Drupal\consumers\Negotiator;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\simple_oauth_refresh_token_buffer\TempStore\RefreshTokenBufferTempStoreFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Http middleware to handle refresh token requests.
 */
class RefreshTokenBufferMiddleware implements HttpKernelInterface {

  public function __construct(
    protected readonly HttpKernelInterface $httpKernel,
    #[Autowire(service: 'consumer.negotiator')]
    protected readonly Negotiator $negotiator,
    protected readonly RefreshTokenBufferTempStoreFactory $tempStoreFactory,
    #[Autowire(service: 'lock')]
    protected readonly LockBackendInterface $lock,
    #[Autowire(service: 'logger.channel.simple_oauth_refresh_token_buffer')]
    protected readonly LoggerInterface $logger,
  ) {}

  /**
   * {@inheritdoc}
   *
   * Handles request to /oauth/token and buffers the response if the
   * consumer has enabled the refresh token buffer.
   */
  public function handle(Request $request, int $type = self::MAIN_REQUEST, bool $catch = TRUE): Response {
    if ($this->isRequestSupported($request)) {
      return $this->handleTokenRefresh($request, $type, $catch);
    }

    return $this->httpKernel->handle($request, $type, $catch);
  }

  /**
   * Handle token refresh request.
   *
   * Makes sure that the request is handled only once and that the response is
   * cached if the request is successful. If the same request has already
   * been handled, the cached response is returned.
   */
  public function handleTokenRefresh(Request $request, int $type = self::MAIN_REQUEST, bool $catch = TRUE) {
    $reqKey = hash('sha256', json_encode($request->request->all()));

    // Try to acquire the lock.
    while (!$this->lock->acquire($reqKey)) {
      // If we can't acquire the lock, wait for it.
      if ($this->lock->wait($reqKey, 30)) {
        // Timeout reached after 30 seconds.
        return new Response(NULL, 408);
      }
    }

    try {
      $consumer = $this->negotiator->negotiateFromRequest($request);
      $gracePeriod = (int) $consumer->get('refresh_token_buffer_grace_period')->getString();
      $tempstore = $this->tempStoreFactory->get($consumer->getClientId(), $gracePeriod);

      // Use cached response.
      if ($response = $tempstore->get($reqKey)) {
        $response->headers->set('X-Buffered', '1');
        return $response;
      }

      // Run actual request.
      $response = $this->httpKernel->handle($request, $type, $catch);

      // Cache successful responses.
      if ($response->getStatusCode() === 200) {
        $tempstore->set($reqKey, $response);
      }

      return $response;
    } finally {
      $this->lock->release($reqKey);
    }
  }

  /**
   * Checks if request is supported.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The incoming request.
   */
  protected function isRequestSupported(Request $request): bool {
    // Only handle requests to /oauth/token.
    if ($request->getPathInfo() !== '/oauth/token') {
      return FALSE;
    }

    if ($request->getContentTypeFormat() !== "form") {
      return FALSE;
    }

    // Only handle refresh_token grant requests.
    $grantType = $request->request->get('grant_type');
    if ($grantType !== 'refresh_token') {
      return FALSE;
    }

    // Check if enabled for consumer.
    $consumer = $this->negotiator->negotiateFromRequest($request);
    if (!$consumer) {
      return FALSE;
    }

    // The refresh token buffer must be enabled for the consumer.
    $enabledField = $consumer->get('refresh_token_buffer_enabled');
    if ($enabledField->isEmpty() || $enabledField->getString() !== '1') {
      return FALSE;
    }

    // The grace period must be greater than 0.
    $gracePeriod = (int) $consumer->get('refresh_token_buffer_grace_period')->getString() ?? 0;
    if ($gracePeriod <= 0) {
      return FALSE;
    }

    return TRUE;
  }

}
