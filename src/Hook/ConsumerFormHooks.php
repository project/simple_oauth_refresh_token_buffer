<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_refresh_token_buffer\Hook;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Consumer form hooks.
 */
class ConsumerFormHooks {

  /**
   * Alter consumer form.
   */
  #[Hook('form_consumer_form_alter')]
  public function alterConsumerForm(array &$form, FormStateInterface $formState, string $formId) {
    // Other modules may have already added the additional settings tab.
    if (!isset($form['additional_settings'])) {
      $form['additional_settings'] = [
        '#type' => 'vertical_tabs',
        '#title' => new TranslatableMarkup('Additional Settings'),
        '#weight' => 7,
      ];
    }

    $stateVisibleIfRefreshTokenGrantEnabled = [
      '#states' => [
        'visible' => [
          ':input[name="grant_types[refresh_token]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    $form['refresh_token_buffer_settings'] = [
      '#type' => 'details',
      '#title' => new TranslatableMarkup('Refresh Token Buffer Settings'),
      '#group' => 'additional_settings',
      '#open' => TRUE,
      '#weight' => 3.7,
      'refresh_token_buffer_settings_info' => [
        '#type' => 'fieldgroup',
        'info' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => new TranslatableMarkup('The <strong>Refresh Token</strong> grant must be enabled to show refresh token buffer settings.'),
          '#attributes' => [
            'class' => ['messages', 'messages--warning'],
          ],
        ],
        '#states' => [
          'visible' => [
            ':input[name="grant_types[refresh_token]"]' => [
              'checked' => FALSE,
            ],
          ],
        ],
      ],
      'refresh_token_buffer_enabled' => array_merge(
        $form['refresh_token_buffer_enabled'],
        $stateVisibleIfRefreshTokenGrantEnabled,
      ),
      'refresh_token_buffer_grace_period' => array_merge(
        $form['refresh_token_buffer_grace_period'],
        $stateVisibleIfRefreshTokenGrantEnabled,
      ),
    ];

    unset($form['refresh_token_buffer_enabled']);
    unset($form['refresh_token_buffer_grace_period']);

    $form['#validate'][] = [$this, 'validateGracePeriod'];
  }

  /**
   * Validates the grace period value.
   */
  public function validateGracePeriod(array &$form, FormStateInterface $formState): void {
    if ($formState->getValue('refresh_token_buffer_enabled')) {
      $gracePeriod = $formState->getValue('refresh_token_buffer_grace_period')[0]['value'];
      if ($gracePeriod < 0 || $gracePeriod > 60) {
        $formState->setErrorByName(
          'refresh_token_buffer_grace_period',
          new TranslatableMarkup('The grace period must be between 0 and 60 minutes.')
        );
      }
    }
  }

}
