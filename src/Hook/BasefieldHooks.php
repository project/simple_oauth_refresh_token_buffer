<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_refresh_token_buffer\Hook;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Basefield hooks.
 */
class BasefieldHooks {

  /**
   * Add consumer base fields.
   */
  #[Hook('entity_base_field_info')]
  public function addConsumerBaseFields(EntityTypeInterface $entityType) {
    $fields = [];

    if ($entityType->id() === 'consumer') {
      $fields['refresh_token_buffer_enabled'] = BaseFieldDefinition::create('boolean')
        ->setLabel(new TranslatableMarkup('Enable Refresh Token Buffer'))
        ->setDescription(new TranslatableMarkup('If enabled, token refresh responses are buffered for a short time to avoid race conditions.'))
        ->setDisplayOptions('view', [
          'label' => 'inline',
          'weight' => 6,
        ])
        ->setDisplayOptions('form', [
          'weight' => 6,
        ])
        ->setRevisionable(TRUE)
        ->setTranslatable(FALSE)
        ->setRequired(FALSE)
        ->setDefaultValue(FALSE);

      $fields['refresh_token_buffer_grace_period'] = BaseFieldDefinition::create('integer')
        ->setLabel(new TranslatableMarkup('Refresh Token Buffer Grace Period'))
        ->setDescription(new TranslatableMarkup('Time in seconds after a token rotation where the previous token still remains valid to allow clients to get the new token.'))
        ->setDisplayOptions('view', [
          'label' => 'inline',
          'weight' => 6,
        ])
        ->setDisplayOptions('form', [
          'weight' => 6,
        ])
        ->setRevisionable(FALSE)
        ->setTranslatable(FALSE)
        ->setRequired(FALSE)
        ->setDefaultValue(30);
    }

    return $fields;
  }

}
