<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_refresh_token_buffer\TempStore;

use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TempStore\SharedTempStore;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Factory for creating temp stores for refresh token buffering.
 *
 * This factory is needed to allow for a configurable expiration time for the
 * items in the store and to use the client ID as the owner of the store.
 */
class RefreshTokenBufferTempStoreFactory {

  public function __construct(
    #[Autowire(service: 'keyvalue.expirable')]
    protected readonly KeyValueExpirableFactoryInterface $storageFactory,
    #[Autowire(service: 'lock')]
    protected readonly LockBackendInterface $lockBackend,
    protected readonly RequestStack $requestStack,
    protected readonly AccountProxyInterface $currentUser,
    #[Autowire(param: 'simple_oauth_refresh_token_buffer.tempstore_collection')]
    protected readonly string $collection,
  ) {}

  /**
   * Get a temp store for a given client ID.
   *
   * This is similar to the SharedTempStoreFactory::get() method, but it
   * allows to set a configurable expiration time for the items in the store
   * and uses the client ID as the owner of the store.
   */
  public function get(string $clientId, int $expire) {
    $storage = $this->storageFactory->get("tempstore.shared." . $this->collection);

    return new SharedTempStore(
      $storage,
      $this->lockBackend,
      $clientId,
      $this->requestStack,
      $this->currentUser,
      $expire,
    );
  }

}
